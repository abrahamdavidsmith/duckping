# duckping

A ping monitor insprired by SmokePing https://oss.oetiker.ch/smokeping/
but written in python for easy modern deployment.

## install

 - use the `install.sh` script.

## Usage

 - `python -m duckping --name quad9 --addr http://quad9.com/ --proto http --tsv out.tsv &`
 - `python src/pkg_duckping/duckping/display.py --tsv out.tsv --png out.png`


