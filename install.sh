#!/bin/bash
conda env create -f duckping_env.yml
source activate duckping
# rm dist/*
# python -m build
# python -m twine upload --user __token__ --password  pypi-XXXXX --repository testpypi  dist/*
pip install -U $(./dependencies.py)
pip install -U -i https://test.pypi.org/simple/  --no-deps duckping
