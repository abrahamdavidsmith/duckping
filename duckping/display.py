import matplotlib.pyplot as plt
import pandas as pd
import json
from itertools import chain
import datetime

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--tsv', help='TSV file for records', type=str)
parser.add_argument('--png', help='PNG file for output', type=str)
args = parser.parse_args()

df = pd.read_csv(args.tsv, sep='\t', names=['log', 'name', 'time', 'mean', 'count', 'rtts'],
                 parse_dates=['time'])


classes = df['name'].unique()

fig,ax = plt.subplots(len(classes), figsize=(6,len(classes)*3), dpi=100, sharex=True)

def plotme(dataframe, axis, name):
    long_time = list(chain([t]*c for t,c in zip(dataframe['time'], dataframe['count'])))
    long_rtts = list(chain(json.loads(r) for r in dataframe['rtts']))
    axis.plot(dataframe['time'], dataframe['mean'], label=name)
    axis.scatter(long_time, long_rtts, marker='x', color='black', alpha=0.5)
    axis.legend()

if len(classes) == 1:
    c = classes[0] 
    plotme(df, ax, c) 
    ax.set_title(datetime.datetime.now())
else:
    for i,c in enumerate(classes):
        subdf = df[df['name'] == c]
        plotme(subdf, ax[i], c)
    ax[0].set_title(datetime.datetime.now())

fig.tight_layout()
plt.savefig(args.png)

