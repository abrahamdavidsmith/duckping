from .config import Host
import argparse
from .record import runhost


parser = argparse.ArgumentParser()
#parser.add_argument('--config', help='JSON file with host configuration', type=str)
parser.add_argument('--name', help='host name', type=str)
parser.add_argument('--addr', help='host address', type=str)
parser.add_argument('--proto', help="protocol", type=str)
parser.add_argument('--tsv', help='TSV file for records', type=str)
args = parser.parse_args()



host = Host(name=args.name, addr=args.addr, proto=args.proto)
print(host)
runhost(host, args.tsv)

#config_list = json.load(open(args.config, 'r'))
#with ProcessPoolExecutor() as ex:
#with Pool() as ex:
 #   rows = ex.map(runhost, config_list)
