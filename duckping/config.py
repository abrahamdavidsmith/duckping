from datetime import datetime
from pydantic import BaseModel
from typing import List

class Host(BaseModel):
    name: str
    addr: str
    proto: str
    freq_secs: int = 55
    burst_ct: int = 5

class Report(BaseModel):
    host: Host
    time: datetime
    mean: float
    rtts: List[float]

    def row(self):
        return f"\t{self.host.name}\t{self.time}\t{self.mean}\t{self.host.burst_ct}\t{sorted(self.rtts)}"