from .config import Host, Report
import requests, icmplib
import datetime
import numpy as np
import time
import logging


def oneburst(host_data:Host):
    t = datetime.datetime.now()
    if host_data.proto.lower() == 'icmp':
        o = icmplib.ping(host_data.addr, count=host_data.burst_ct, interval=1, privileged=False)
        return Report(host=host_data, time=t, mean=o.avg_rtt, rtts=o.rtts)

    elif host_data.proto.lower() == 'http':
        rtts = []
        assert host_data.addr.lower().startswith("http")
        for i in range(host_data.burst_ct):
            t0 = time.time()
            try:
                requests.head(host_data.addr)
                rtts.append((time.time() - t0)*1e3) 
            except requests.exceptions.ConnectionError:
                rtts.append(np.inf) 
            time.sleep(1)
        return Report(host=host_data, time=t, mean=np.mean(rtts), rtts=rtts)

    else:
        raise ValueError(f"Only protocols are 'icmp' and 'http', not '{host_data.proto}'")


def runhost(host, logfile):
    # Use the logging facility to allow clean file appending 
    log_out = logging.getLogger()
    log_out.setLevel(logging.INFO)
    log_handler = logging.FileHandler(logfile, 'w', 'utf-8')
    log_handler.setFormatter(logging.Formatter('%(name)s %(message)s'))
    log_out.addHandler(log_handler)
    while True:
        out = oneburst(host)
        row = out.row()
        log_out.info(row)
        time.sleep(host.freq_secs)
