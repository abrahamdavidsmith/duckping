#!/bin/env python3

import toml
import pathlib
pyproject_path = pathlib.Path("pyproject.toml")

print(" ".join(toml.load(pyproject_path.open())["project"]["dependencies"]))

